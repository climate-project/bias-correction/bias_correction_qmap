library(ggplot2)
library(tidyverse)
library(qmap)
library(hydroGOF)
library(EnvStats)

file_name <- "TH000048327_TMIN"
obs_index <- 'TMIN'
mod_index <- 'tasmin'


df <- read_csv(sprintf("./data/%s.csv", file_name))

train <- df %>% filter(DATE < "2000-01-01")
test <- df %>% filter(DATE >= "2000-01-01")

qmap_methods <- c("PTF", "RQUANT","QUANT","SSPLIN")

for (method in qmap_methods) {
  qm <- fitQmap(train[obs_index], train[mod_index], method=method)
  train_corrected <- doQmap(train[mod_index], qm)
  test_corrected <- doQmap(test[mod_index], qm)
  
  print(method)
  print(sprintf("Train MAE: %.2f -> %.2f", mae(train[mod_index], train[, obs_index]), mae(train_corrected, train[obs_index])))
  print(sprintf("Test MAE:  %.2f -> %.2f", mae(test[mod_index], test[, obs_index]), mae(test_corrected, test[obs_index])))
  
  
  df[method] <- round(doQmap(df[, mod_index], qm), 2)
}
  

write.csv(df, sprintf("./results/%s.csv", file_name), row.names=FALSE)